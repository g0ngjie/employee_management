package com.service;

import com.dao.CommonDaoImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class CommonService {

    @Autowired
    private CommonDaoImpl commonDao;

    //登陆验证
    public Map<String, Object> loginAuth(String loginName, String passwd) throws Exception {
        Map<String, Object> findUser = commonDao.findUserByLoginParams(loginName, passwd);
        if (findUser != null) {
            return findUser;
        }
        return null;
    }

    public Map<String, Object> userInfoById(String userId) throws Exception {
        Map<String, Object> findUser = commonDao.findUserById(userId);
        if (findUser != null) {
            return findUser;
        }
        return null;
    }
}
