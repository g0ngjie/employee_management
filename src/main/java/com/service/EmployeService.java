package com.service;

import com.common.BaseResult;
import com.dao.CommonDaoImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class EmployeService extends BaseResult {

    @Autowired
    private CommonDaoImpl commonDao;

    public Map<String, Object> queryList(Integer start, Integer limit, String name, String sex, String user_no) {
        try {
            Map map = new HashMap();
            List<Object> list = commonDao.queryEmplList(start, limit, name, sex, user_no);
            Integer count = commonDao.queryCountEmplList(start, limit,  name, sex, user_no);
            map.put("list", list);
            map.put("count", count);
            return resultOk(map);
        } catch (Exception e) {
            return resultErr(e.getMessage());
        }
    }

    public Map<String, Object> emplAdd(String name, String sex, String login_name, String passwd, String user_no, String email, String dept_name, String type) {
        try {
            commonDao.createEmpl(name, sex, login_name, passwd, user_no, email, dept_name, type);
            return resultOk("添加成功");
        } catch (Exception e) {
            return resultErr(e.getMessage());
        }
    }

    public Map<String, Object> emplEdit(Integer id, String user_name, String sex, String login_name, String passwd, String user_no, String email, String dept_name, String type) {
        try {
            commonDao.delEmplById(id);
            commonDao.createEmpl(user_name, sex, login_name, passwd, user_no, email, dept_name, type);
            return resultOk("修改成功");
        } catch (Exception e) {
            return resultErr(e.getMessage());
        }
    }

    public Map<String, Object> emplDel(Integer id) {
        try {
            commonDao.delEmplById(id);
            return resultOk("刪除成功");
        } catch (Exception e) {
            return resultErr(e.getMessage());
        }
    }

    public Object listAll() {
        try {
            List<Object> list = commonDao.findAll();
            return resultOk(list);
        } catch (Exception e) {
            return resultErr(e.getMessage());
        }
    }

    public Map<String, Object> queryById(Integer userId) throws Exception {
        return commonDao.queryById(userId);
    }
}
