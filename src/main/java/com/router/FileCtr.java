package com.router;

import com.common.BaseResult;
import com.proxy.FileProxy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/api/file")
public class FileCtr extends BaseResult {

    @Autowired
    private FileProxy fileProxy;

    @PostMapping("/list")
    @ResponseBody
    public Object list(@RequestBody Map<String, Object> res) {
        Map<String, Object> map = new HashMap();
        try {
            Integer start = (Integer) res.get("start");
            Integer limit = (Integer) res.get("limit");
            String name = (String) res.get("name");
            String type = (String) res.get("type");
            String user_name = (String) res.get("user_name");
            String user_id = (String) res.get("user_id");
            String dept_name = (String) res.get("dept_name");
            String dept_id = (String) res.get("dept_id");
            List<Object> list = fileProxy.queryList(start, limit, name, type, user_name, user_id, dept_name, dept_id);
            Integer count = fileProxy.queryCount(name, type, user_name, user_id, dept_name, dept_id);
            map.put("list", list);
            map.put("count", count);
            return resultOk(map);
        } catch (Exception e) {
            return resultErr(e.getMessage());
        }

    }

    @PostMapping("/findById")
    @ResponseBody
    public Object findById(@RequestBody Map<String, Object> res) {
        try {
            String id = (String) res.get("id");
            Map<String, Object> findOne = fileProxy.queryById(Integer.valueOf(id));
            return resultOk(findOne);
        } catch (Exception e) {
            return resultErr(e.getMessage());
        }
    }

    @PostMapping("/update")
    @ResponseBody
    public Object update(@RequestBody Map<String, Object> res) {
        try {
            String id = (String) res.get("id");
            String name = (String) res.get("name");
            String type = (String) res.get("type");
            String user_name = (String) res.get("user_name");
            String user_id = (String) res.get("user_id");
            String dept_name = (String) res.get("dept_name");
            String dept_id = (String) res.get("dept_id");
            fileProxy.delete(Integer.valueOf(id));
            fileProxy.create(name, type, user_name, user_id, dept_name, dept_id);
            return resultOk("ok");
        } catch (Exception e) {
            return resultErr(e.getMessage());
        }
    }

    @PostMapping("/create")
    @ResponseBody
    public Object create(@RequestBody Map<String, Object> res) {
        try {
            String name = (String) res.get("name");
            String type = (String) res.get("type");
            String user_name = (String) res.get("user_name");
            String user_id = (String) res.get("user_id");
            String dept_name = (String) res.get("dept_name");
            String dept_id = (String) res.get("dept_id");
            fileProxy.create(name, type, user_name, user_id, dept_name, dept_id);
            return resultOk("ok");
        } catch (Exception e) {
            return resultErr(e.getMessage());
        }
    }

    @PostMapping("/delete")
    @ResponseBody
    public Object delete(@RequestBody Map<String, Object> res) {
        try {
            String id = (String) res.get("id");
            fileProxy.delete(Integer.valueOf(id));
            return resultOk("ok");
        } catch (Exception e) {
            return resultErr(e.getMessage());
        }
    }

}
