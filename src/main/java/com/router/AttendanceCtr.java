package com.router;

import com.common.BaseResult;
import com.proxy.AttendanceProxy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/api/attendance")
public class AttendanceCtr extends BaseResult {

    @Autowired
    private AttendanceProxy attendanceProxy;

    @PostMapping("/list")
    @ResponseBody
    public Object list(@RequestBody Map<String, Object> res) {
        Map<String, Object> map = new HashMap();
        try {
            Integer start = (Integer) res.get("start");
            Integer limit = (Integer) res.get("limit");
            String user_name = (String) res.get("user_name");
            String user_no = (String) res.get("user_no");
            List<Object> list = attendanceProxy.queryList(start, limit, user_name, user_no);
            Integer count = attendanceProxy.queryCount(user_name, user_no);
            map.put("list", list);
            map.put("count", count);
            return resultOk(map);
        } catch (Exception e) {
            return resultErr(e.getMessage());
        }

    }

    @PostMapping("/findById")
    @ResponseBody
    public Object findById(@RequestBody Map<String, Object> res) {
        try {
            String id = (String) res.get("id");
            Map<String, Object> findOne = attendanceProxy.queryById(Integer.valueOf(id));
            return resultOk(findOne);
        } catch (Exception e) {
            return resultErr(e.getMessage());
        }
    }

    @PostMapping("/update")
    @ResponseBody
    public Object update(@RequestBody Map<String, Object> res) {
        try {
            String id = (String) res.get("id");
            String user_name = (String) res.get("user_name");
            String user_no = (String) res.get("user_no");
            String start_work_time = (String) res.get("start_work_time");
            attendanceProxy.delete(Integer.valueOf(id));
            attendanceProxy.create(user_name, user_no, start_work_time);
            return resultOk("ok");
        } catch (Exception e) {
            return resultErr(e.getMessage());
        }
    }

    @PostMapping("/create")
    @ResponseBody
    public Object create(@RequestBody Map<String, Object> res) {
        try {
            String user_name = (String) res.get("user_name");
            String user_no = (String) res.get("user_no");
            String start_work_time = (String) res.get("start_work_time");
            attendanceProxy.create(user_name, user_no, start_work_time);
            return resultOk("ok");
        } catch (Exception e) {
            return resultErr(e.getMessage());
        }
    }

    @PostMapping("/delete")
    @ResponseBody
    public Object delete(@RequestBody Map<String, Object> res) {
        try {
            String id = (String) res.get("id");
            attendanceProxy.delete(Integer.valueOf(id));
            return resultOk("ok");
        } catch (Exception e) {
            return resultErr(e.getMessage());
        }
    }
}
