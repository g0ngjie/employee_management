package com.router;

import com.common.BaseResult;
import com.proxy.ProjectProxy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/api/project")
public class ProjectCtr extends BaseResult {


    @Autowired
    private ProjectProxy projectProxy;

    @PostMapping("/list")
    @ResponseBody
    public Object list(@RequestBody Map<String, Object> res) {
        Map<String, Object> map = new HashMap();
        try {
            Integer start = (Integer) res.get("start");
            Integer limit = (Integer) res.get("limit");
            String name = (String) res.get("name");
            String user_name = (String) res.get("user_name");
            String start_time = (String) res.get("start_time");
            List<Object> list = projectProxy.queryList(start, limit, name, user_name, start_time);
            Integer count = projectProxy.queryCount(name, user_name, start_time);
            map.put("list", list);
            map.put("count", count);
            return resultOk(map);
        } catch (Exception e) {
            return resultErr(e.getMessage());
        }

    }

    @PostMapping("/findById")
    @ResponseBody
    public Object findById(@RequestBody Map<String, Object> res) {
        try {
            String id = (String) res.get("id");
            Map<String, Object> findOne = projectProxy.queryById(Integer.valueOf(id));
            return resultOk(findOne);
        } catch (Exception e) {
            return resultErr(e.getMessage());
        }
    }

    @PostMapping("/update")
    @ResponseBody
    public Object update(@RequestBody Map<String, Object> res) {
        try {
            String id = (String) res.get("id");
            String name = (String) res.get("name");
            String user_name = (String) res.get("user_name");
            String start_time = (String) res.get("start_time");
            projectProxy.delete(Integer.valueOf(id));
            projectProxy.create(name, user_name, start_time);
            return resultOk("ok");
        } catch (Exception e) {
            return resultErr(e.getMessage());
        }
    }

    @PostMapping("/create")
    @ResponseBody
    public Object create(@RequestBody Map<String, Object> res) {
        try {
            String name = (String) res.get("name");
            String user_name = (String) res.get("user_name");
            String start_time = (String) res.get("start_time");
            projectProxy.create(name, user_name, start_time);
            return resultOk("ok");
        } catch (Exception e) {
            return resultErr(e.getMessage());
        }
    }

    @PostMapping("/delete")
    @ResponseBody
    public Object delete(@RequestBody Map<String, Object> res) {
        try {
            String id = (String) res.get("id");
            projectProxy.delete(Integer.valueOf(id));
            return resultOk("ok");
        } catch (Exception e) {
            return resultErr(e.getMessage());
        }
    }
}
