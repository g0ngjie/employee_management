package com.router;

import com.common.BaseResult;
import com.service.EmployeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

//员工
@Controller
@RequestMapping("/api/employee")
public class EmployeeCtr extends BaseResult {

    @Autowired
    private EmployeService emplService;

    @PostMapping("/list")
    @ResponseBody
    public Object employeeList(@RequestBody Map<String, Object> res) {
        Map<String, Object> map = new HashMap();
        Integer start = (Integer) res.get("start");
        Integer limit = (Integer) res.get("limit");
        String name = (String) res.get("user_name");
        String sex = (String) res.get("sex");
        String user_no = (String) res.get("user_no");
        Object list = emplService.queryList(start, limit, name, sex, user_no);
        return list;
    }

    @PostMapping("/add")
    @ResponseBody
    public Object emplAdd(@RequestBody Map<String, Object> res) {
        String name = (String) res.get("user_name");
        String sex = (String) res.get("sex");
        String type = (String) res.get("type");
        String login_name = (String) res.get("login_name");
        String passwd = (String) res.get("passwd");
        String user_no = (String) res.get("user_no");
        String email = (String) res.get("email");
        String dept_name = (String) res.get("dept_name");
        Map<String, Object> map = emplService.emplAdd(name, sex, login_name, passwd, user_no, email, dept_name, type);
        return map;
    }

    @PostMapping("/update")
    @ResponseBody
    public Object emplEdit(@RequestBody Map<String, Object> res) {
        String id = (String) res.get("id");
        String user_name = (String) res.get("user_name");
        String sex = (String) res.get("sex");
        String type = (String) res.get("type");
        String login_name = (String) res.get("login_name");
        String passwd = (String) res.get("passwd");
        String user_no = (String) res.get("user_no");
        String email = (String) res.get("email");
        String dept_name = (String) res.get("dept_name");
        Map<String, Object> map = emplService.emplEdit(Integer.valueOf(id), user_name,
                sex, login_name, passwd, user_no, email, dept_name, type);
        return map;
    }

    @PostMapping("/delete")
    @ResponseBody
    public Object emplDel(@RequestBody Map<String, Object> res) {
        String id = (String) res.get("id");
        Map<String, Object> map = emplService.emplDel(Integer.valueOf(id));
        return map;
    }

    @PostMapping("/listAll")
    @ResponseBody
    public Object emplListAll() {
        Object list = emplService.listAll();
        return list;
    }

    @PostMapping("/findById")
    @ResponseBody
    public Object findById(@RequestBody Map<String, Object> res) {
        try {
            String id = (String) res.get("id");
            Map<String, Object> findOne = emplService.queryById(Integer.valueOf(id));
            return resultOk(findOne);
        } catch (Exception e) {
            return resultErr(e.getMessage());
        }
    }
}
