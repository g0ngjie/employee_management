package com.router;

import com.common.BaseResult;
import com.service.CommonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

//公共模块
@Controller
@RequestMapping("/api/common")
public class CommonCtr extends BaseResult {

    @Autowired
    private CommonService commonService;

    @PostMapping("/login")
    @ResponseBody
    public Map<String, Object> login(@RequestBody Map<String, Object> res) {
        Map<String, Object> map = new HashMap<String, Object>();
        String login_name = (String) res.get("login_name");
        String passwd = (String) res.get("passwd");
        try {
            Map<String, Object> user = commonService.loginAuth(login_name, passwd);
            if(user != null) return resultOk(user);
            else return userNotFound("用户不存在");
        } catch (Exception e) {
            return resultErr(e.getMessage());
        }
    }

    @PostMapping("/userInfo")
    @ResponseBody
    public Map<String, Object> userInfo(@RequestBody Map<String, Object> res) {
        Map<String, Object> map = new HashMap();
        String userId = (String) res.get("userId");
        try {
            Map<String, Object> user = commonService.userInfoById(userId);
            if(user != null) return resultOk(user);
            else return userNotFound("用户不存在");
        } catch (Exception e) {
            return resultErr(e.getMessage());
        }
    }


}
