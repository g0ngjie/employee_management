package com.router;

import com.common.BaseResult;
import com.proxy.AskForLeaveProxy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/api/askForLeave")
public class AskForLeaveCtr extends BaseResult {

    @Autowired
    private AskForLeaveProxy askForLeaveProxy;

    @PostMapping("/list")
    @ResponseBody
    public Object list(@RequestBody Map<String, Object> res) {
        Map<String, Object> map = new HashMap();
        try {
            Integer start = (Integer) res.get("start");
            Integer limit = (Integer) res.get("limit");
            String user_name = (String) res.get("user_name");
            String user_id = (String) res.get("user_id");
            String start_time = (String) res.get("start_time");
            String type = (String) res.get("type");
            String remark = (String) res.get("remark");
            List<Object> list = askForLeaveProxy.queryList(start, limit, user_name, user_id, start_time, type, remark);
            Integer count = askForLeaveProxy.queryCount(user_name, user_id, start_time, type, remark);
            map.put("list", list);
            map.put("count", count);
            return resultOk(map);
        } catch (Exception e) {
            return resultErr(e.getMessage());
        }

    }

    @PostMapping("/findById")
    @ResponseBody
    public Object findById(@RequestBody Map<String, Object> res) {
        try {
            String id = (String) res.get("id");
            Map<String, Object> findOne = askForLeaveProxy.queryById(Integer.valueOf(id));
            return resultOk(findOne);
        } catch (Exception e) {
            return resultErr(e.getMessage());
        }
    }

    @PostMapping("/update")
    @ResponseBody
    public Object update(@RequestBody Map<String, Object> res) {
        try {
            String id = (String) res.get("id");
            String user_name = (String) res.get("user_name");
            String user_id = (String) res.get("user_id");
            String start_time = (String) res.get("start_time");
            String type = (String) res.get("type");
            String remark = (String) res.get("remark");
            askForLeaveProxy.delete(Integer.valueOf(id));
            askForLeaveProxy.create(user_name, user_id, start_time, type, remark);
            return resultOk("ok");
        } catch (Exception e) {
            return resultErr(e.getMessage());
        }
    }

    @PostMapping("/create")
    @ResponseBody
    public Object create(@RequestBody Map<String, Object> res) {
        try {
            String user_name = (String) res.get("user_name");
            String user_id = (String) res.get("user_id");
            String start_time = (String) res.get("start_time");
            String type = (String) res.get("type");
            String remark = (String) res.get("remark");
            askForLeaveProxy.create(user_name, user_id, start_time, type, remark);
            return resultOk("ok");
        } catch (Exception e) {
            return resultErr(e.getMessage());
        }
    }

    @PostMapping("/delete")
    @ResponseBody
    public Object delete(@RequestBody Map<String, Object> res) {
        try {
            String id = (String) res.get("id");
            askForLeaveProxy.delete(Integer.valueOf(id));
            return resultOk("ok");
        } catch (Exception e) {
            return resultErr(e.getMessage());
        }
    }
}
