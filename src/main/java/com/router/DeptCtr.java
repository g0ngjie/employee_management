package com.router;

import com.common.BaseResult;
import com.proxy.DeptProxy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/api/dept")
public class DeptCtr extends BaseResult {

    @Autowired
    private DeptProxy deptProxy;

    @PostMapping("/list")
    @ResponseBody
    public Object list(@RequestBody Map<String, Object> res) {
        Map<String, Object> map = new HashMap();
        try {
            Integer start = (Integer) res.get("start");
            Integer limit = (Integer) res.get("limit");
            String name = (String) res.get("name");
            String number = (String) res.get("number");
            String remark = (String) res.get("remark");
            String user_name = (String) res.get("user_name");
            List<Object> list = deptProxy.queryList(start, limit, name, number, remark, user_name);
            Integer count = deptProxy.queryCount(name, number, remark, user_name);
            map.put("list", list);
            map.put("count", count);
            return resultOk(map);
        } catch (Exception e) {
            return resultErr(e.getMessage());
        }

    }

    @PostMapping("/findById")
    @ResponseBody
    public Object findById(@RequestBody Map<String, Object> res) {
        try {
            String id = (String) res.get("id");
            Map<String, Object> findOne = deptProxy.queryById(Integer.valueOf(id));
            return resultOk(findOne);
        } catch (Exception e) {
            return resultErr(e.getMessage());
        }
    }

    @PostMapping("/update")
    @ResponseBody
    public Object update(@RequestBody Map<String, Object> res) {
        try {
            String id = (String) res.get("id");
            String name = (String) res.get("name");
            String number = (String) res.get("number");
            String remark = (String) res.get("remark");
            String user_name = (String) res.get("user_name");
            deptProxy.delete(Integer.valueOf(id));
            deptProxy.create(name, number, remark, user_name);
            return resultOk("ok");
        } catch (Exception e) {
            return resultErr(e.getMessage());
        }
    }

    @PostMapping("/create")
    @ResponseBody
    public Object create(@RequestBody Map<String, Object> res) {
        try {
            String name = (String) res.get("name");
            String number = (String) res.get("number");
            String remark = (String) res.get("remark");
            String user_name = (String) res.get("user_name");
            deptProxy.create(name, number, remark, user_name);
            return resultOk("ok");
        } catch (Exception e) {
            return resultErr(e.getMessage());
        }
    }

    @PostMapping("/delete")
    @ResponseBody
    public Object delete(@RequestBody Map<String, Object> res) {
        try {
            String id = (String) res.get("id");
            deptProxy.delete(Integer.valueOf(id));
            return resultOk("ok");
        } catch (Exception e) {
            return resultErr(e.getMessage());
        }
    }

    @PostMapping("/listAll")
    @ResponseBody
    public Object listAll() {
        try {
            List<Object> list = deptProxy.findAll();
            return resultOk(list);
        } catch (Exception e) {
            return resultErr(e.getMessage());
        }
    }
}
