package com.common;

import java.util.HashMap;
import java.util.Map;

public class BaseResult {

    public Map<String, Object> resultOk(Object data) {
            Map<String, Object> map = new HashMap();
            map.put("code", 100);
            map.put("data", data);
            return map;
    }

    public Map<String, Object> resultErr(String errMsg) {
        Map<String, Object> map = new HashMap();
        map.put("code", 101);
        map.put("errMsg", errMsg);
        return map;
    }

    public Map<String, Object> userNotFound(String errMsg) {
        Map<String, Object> map = new HashMap();
        map.put("code", 102);
        map.put("errMsg", errMsg);
        return map;
    }
}
