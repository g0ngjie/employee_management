package com.common;

import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

/**
 * 初始化 执行
 */
@Component
public class InitExecute implements CommandLineRunner{

    @Override
    public void run(String... strings) {
        System.out.println("========================================");
        System.out.println("                 项目启动");
        System.out.println("========================================");
    }
}
