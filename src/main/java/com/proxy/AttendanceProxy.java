package com.proxy;

import com.common.BaseDao;
import com.mysql.jdbc.StringUtils;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository
public class AttendanceProxy extends BaseDao {

    public Map<String, Object> queryById(Integer id) throws Exception {
        String sql = "select * from attendance where id = " + id + "";
        List<Object> list = this.getDataBySqlForMysql(sql);
        return list.size() != 0 ? (Map<String, Object>) list.get(0) : null;
    }

    public List<Object> queryList(Integer start, Integer limit, String user_name, String user_no) throws Exception {
        String sql = "select * from attendance where 1 = 1 ";
        if (!StringUtils.isNullOrEmpty(user_name)) {
            sql += " and user_name like '%" + user_name + "%'";
        }
        if (!StringUtils.isNullOrEmpty(user_no)) {
            sql += " and user_no = '" + user_no + "'";
        }
        sql += " order by id desc limit " + start + "," + limit + "";
        return this.getDataBySqlForMysql(sql);
    }

    public Integer queryCount(String user_name, String user_no) throws Exception {
        String sql = "";
        sql = "SELECT count(*) count from attendance where 1 = 1";
        if (!StringUtils.isNullOrEmpty(user_name)) {
            sql += " and user_name like '%" + user_name + "%'";
        }
        if (!StringUtils.isNullOrEmpty(user_no)) {
            sql += " and user_no = '" + user_no + "'";
        }

        Integer count = this.queryCountForMysql(sql);
        return count;
    }

    public void create(String user_name, String user_no, String start_work_time) throws Exception {
        String insertImg = "insert into attendance (user_name,user_no,start_work_time,create_time) " +
                "values('" + user_name + "','" + user_no + "', '"+start_work_time+"','"+System.currentTimeMillis()+"')";
        this.updateForMysql(insertImg);
    }

    public void delete(Integer id) throws Exception {
        String sql = "delete from attendance where id = " + id + "";
        this.updateForMysql(sql);
    }
}
