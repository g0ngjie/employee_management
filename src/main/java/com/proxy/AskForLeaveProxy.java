package com.proxy;

import com.common.BaseDao;
import com.mysql.jdbc.StringUtils;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository
public class AskForLeaveProxy extends BaseDao {

    public Map<String, Object> queryById(Integer id) throws Exception {
        String sql = "select * from ask_for_leave where id = " + id + "";
        List<Object> list = this.getDataBySqlForMysql(sql);
        return list.size() != 0 ? (Map<String, Object>) list.get(0) : null;
    }

    public List<Object> queryList(Integer start, Integer limit,
                                  String user_name, String user_id,
                                  String start_time, String type, String remark) throws Exception {
        String sql = "select * from ask_for_leave where 1 = 1 ";
        if (!StringUtils.isNullOrEmpty(user_name)) {
            sql += " and user_name like '%" + user_name + "%'";
        }
        if (!StringUtils.isNullOrEmpty(user_id)) {
            sql += " and user_id = '" + Integer.valueOf(user_id) + "'";
        }
        if (!StringUtils.isNullOrEmpty(start_time)) {
            sql += " and start_time = '" + start_time + "'";
        }
        if (!StringUtils.isNullOrEmpty(type)) {
            sql += " and type like '%" + type + "%'";
        }
        if (!StringUtils.isNullOrEmpty(remark)) {
            sql += " and remark = '" + remark + "'";
        }
        sql += " order by id desc limit " + start + "," + limit + "";
        return this.getDataBySqlForMysql(sql);
    }

    public Integer queryCount(String user_name, String user_id,
                              String start_time, String type, String remark) throws Exception {
        String sql = "";
        sql = "SELECT count(*) count from ask_for_leave where 1 = 1";
        if (!StringUtils.isNullOrEmpty(user_name)) {
            sql += " and user_name like '%" + user_name + "%'";
        }
        if (!StringUtils.isNullOrEmpty(user_id)) {
            sql += " and user_id = '" + Integer.valueOf(user_id) + "'";
        }
        if (!StringUtils.isNullOrEmpty(start_time)) {
            sql += " and start_time = '" + start_time + "'";
        }
        if (!StringUtils.isNullOrEmpty(type)) {
            sql += " and type like '%" + type + "%'";
        }
        if (!StringUtils.isNullOrEmpty(remark)) {
            sql += " and remark = '" + remark + "'";
        }
        Integer count = this.queryCountForMysql(sql);
        return count;
    }

    public void create(String user_name, String user_id,
                       String start_time, String type, String remark) throws Exception {
        String insertImg = "insert into ask_for_leave (user_name,user_id,start_time,type,remark,create_time) " +
                "values('" + user_name + "'," + Integer.valueOf(user_id) + ", '"+start_time+"','"+type+"','"+remark+"','"+System.currentTimeMillis()+"')";
        this.updateForMysql(insertImg);
    }

    public void delete(Integer id) throws Exception {
        String sql = "delete from ask_for_leave where id = " + id + "";
        this.updateForMysql(sql);
    }

}
