package com.proxy;

import com.common.BaseDao;
import com.mysql.jdbc.StringUtils;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository
public class ProjectProxy extends BaseDao {

    public Map<String, Object> queryById(Integer id) throws Exception {
        String sql = "select * from project where id = " + id + "";
        List<Object> list = this.getDataBySqlForMysql(sql);
        return list.size() != 0 ? (Map<String, Object>) list.get(0) : null;
    }

    public List<Object> queryList(Integer start, Integer limit, String name, String user_name, String start_time) throws Exception {
        String sql = "select * from project where 1 = 1 ";
        if (!StringUtils.isNullOrEmpty(user_name)) {
            sql += " and user_name like '%" + user_name + "%'";
        }
        if (!StringUtils.isNullOrEmpty(name)) {
            sql += " and name like '%" + name + "%'";
        }
        if (!StringUtils.isNullOrEmpty(start_time)) {
            sql += " and start_time = '" + start_time + "'";
        }
        sql += " order by id desc limit " + start + "," + limit + "";
        return this.getDataBySqlForMysql(sql);
    }

    public Integer queryCount(String name, String user_name, String start_time) throws Exception {
        String sql = "";
        sql = "SELECT count(*) count from project where 1 = 1";
        if (!StringUtils.isNullOrEmpty(user_name)) {
            sql += " and user_name like '%" + user_name + "%'";
        }
        if (!StringUtils.isNullOrEmpty(name)) {
            sql += " and name like '%" + name + "%'";
        }
        if (!StringUtils.isNullOrEmpty(start_time)) {
            sql += " and start_time = '" + start_time + "'";
        }
        Integer count = this.queryCountForMysql(sql);
        return count;
    }

    public void create(String name, String user_name, String start_time) throws Exception {
        String insertImg = "insert into project (name,user_name,start_time,create_time) " +
                "values('" + name + "','" + user_name + "', '"+start_time+"','"+System.currentTimeMillis()+"')";
        this.updateForMysql(insertImg);
    }

    public void delete(Integer id) throws Exception {
        String sql = "delete from project where id = " + id + "";
        this.updateForMysql(sql);
    }
}
