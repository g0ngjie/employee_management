package com.proxy;

import com.common.BaseDao;
import com.mysql.jdbc.StringUtils;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository
public class DeptProxy extends BaseDao{

    public Map<String, Object> queryById(Integer id) throws Exception {
        String sql = "select * from dept where id = " + id + "";
        List<Object> list = this.getDataBySqlForMysql(sql);
        return list.size() != 0 ? (Map<String, Object>) list.get(0) : null;
    }

    public List<Object> queryList(Integer start, Integer limit, String name, String number, String remark, String user_name) throws Exception {
        String sql = "select * from dept where 1 = 1 ";
        if (!StringUtils.isNullOrEmpty(name)) {
            sql += " and name like '%" + name + "%'";
        }
        if (!StringUtils.isNullOrEmpty(number)) {
            sql += " and number like '%" + number + "%'";
        }
        if (!StringUtils.isNullOrEmpty(remark)) {
            sql += " and remark like '%" + remark + "%'";
        }
        if (!StringUtils.isNullOrEmpty(user_name)) {
            sql += " and user_name like '%" + user_name + "%'";
        }
        sql += " order by id desc limit " + start + "," + limit + "";
        return this.getDataBySqlForMysql(sql);
    }

    public Integer queryCount(String name, String number, String remark, String user_name) throws Exception {
        String sql = "";
        sql = "SELECT count(*) count from dept where 1 = 1";
        if (!StringUtils.isNullOrEmpty(name)) {
            sql += " and name like '%" + name + "%'";
        }
        if (!StringUtils.isNullOrEmpty(number)) {
            sql += " and number like '%" + number + "%'";
        }
        if (!StringUtils.isNullOrEmpty(remark)) {
            sql += " and remark like '%" + remark + "%'";
        }
        if (!StringUtils.isNullOrEmpty(user_name)) {
            sql += " and user_name like '%" + user_name + "%'";
        }
        Integer count = this.queryCountForMysql(sql);
        return count;
    }

    public void create(String name, String number, String remark, String user_name) throws Exception {
        String insertImg = "insert into dept (name,number,remark,user_name,create_time) " +
                "values('" + name + "','" + number + "', '"+remark+"','"+user_name+"','"+System.currentTimeMillis()+"')";
        this.updateForMysql(insertImg);
    }

    public void delete(Integer id) throws Exception {
        String sql = "delete from dept where id = " + id + "";
        this.updateForMysql(sql);
    }

    public List<Object> findAll() throws Exception {
        String sql = "select * from dept where 1 = 1 ";
        sql += " order by id desc";
        return this.getDataBySqlForMysql(sql);
    }

}
