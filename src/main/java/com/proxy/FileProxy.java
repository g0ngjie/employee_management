package com.proxy;

import com.common.BaseDao;
import com.mysql.jdbc.StringUtils;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository
public class FileProxy extends BaseDao {

    public Map<String, Object> queryById(Integer id) throws Exception {
        String sql = "select * from file where id = " + id + "";
        List<Object> list = this.getDataBySqlForMysql(sql);
        return list.size() != 0 ? (Map<String, Object>) list.get(0) : null;
    }

    public List<Object> queryList(Integer start, Integer limit,
                                  String name, String type, String user_name, String user_id,
                                  String dept_name, String dept_id) throws Exception {
        String sql = "select * from file where 1 = 1 ";
        if (!StringUtils.isNullOrEmpty(name)) {
            sql += " and name like '%" + name + "%'";
        }
        if (!StringUtils.isNullOrEmpty(type)) {
            sql += " and type = '" + type + "'";
        }
        if (!StringUtils.isNullOrEmpty(user_name)) {
            sql += " and user_name like '%" + user_name + "%'";
        }
        if (!StringUtils.isNullOrEmpty(user_id)) {
            sql += " and user_id = '" + Integer.valueOf(user_id) + "'";
        }
        if (!StringUtils.isNullOrEmpty(dept_name)) {
            sql += " and dept_name like '%" + dept_name + "%'";
        }
        if (!StringUtils.isNullOrEmpty(dept_id)) {
            sql += " and dept_id = '" + Integer.valueOf(dept_id) + "'";
        }
        sql += " order by id desc limit " + start + "," + limit + "";
        return this.getDataBySqlForMysql(sql);
    }

    public Integer queryCount(String name, String type, String user_name, String user_id,
                              String dept_name, String dept_id) throws Exception {
        String sql = "";
        sql = "SELECT count(*) count from file where 1 = 1";
        if (!StringUtils.isNullOrEmpty(name)) {
            sql += " and name like '%" + name + "%'";
        }
        if (!StringUtils.isNullOrEmpty(type)) {
            sql += " and type = '" + type + "'";
        }
        if (!StringUtils.isNullOrEmpty(user_name)) {
            sql += " and user_name like '%" + user_name + "%'";
        }
        if (!StringUtils.isNullOrEmpty(user_id)) {
            sql += " and user_id = '" + Integer.valueOf(user_id) + "'";
        }
        if (!StringUtils.isNullOrEmpty(dept_name)) {
            sql += " and dept_name like '%" + dept_name + "%'";
        }
        if (!StringUtils.isNullOrEmpty(dept_id)) {
            sql += " and dept_id = '" + Integer.valueOf(dept_id) + "'";
        }
        Integer count = this.queryCountForMysql(sql);
        return count;
    }

    public void create(String name, String type, String user_name, String user_id,
                       String dept_name, String dept_id) throws Exception {
        String insertImg = "insert into file (name,type,user_name,user_id,dept_name,dept_id,create_time) " +
                "values('" + name + "','" + type + "', '"+user_name+"',"+user_id+",'"+dept_name+"',"+dept_id+",'"+System.currentTimeMillis()+"')";
        this.updateForMysql(insertImg);
    }

    public void delete(Integer id) throws Exception {
        String sql = "delete from file where id = " + id + "";
        this.updateForMysql(sql);
    }
}
