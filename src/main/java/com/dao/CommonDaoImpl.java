package com.dao;

import com.common.BaseDao;
import com.mysql.jdbc.StringUtils;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository
public class CommonDaoImpl extends BaseDao {

    public Map<String, Object> findUserByLoginParams(String loginName, String passwd) throws Exception {
        String sql = "select * from user where login_name = '" + loginName + "' and passwd = '" + passwd + "'";
        List<Object> list = this.getDataBySqlForMysql(sql);
        return list.size() != 0 ? (Map<String, Object>) list.get(0) : null;
    }

    public Map<String, Object> findUserById(String userId) throws Exception {
        String sql = "select * from user where id = '" + userId + "'";
        List<Object> list = this.getDataBySqlForMysql(sql);
        return list.size() != 0 ? (Map<String, Object>) list.get(0) : null;
    }

    public Integer queryCountEmplList(Integer start, Integer limit, String name, String sex, String user_no) throws Exception {
        String sql = "SELECT count(*) count from user where 1 = 1";
        if (!StringUtils.isNullOrEmpty(name)) {
            sql += " and user_name like '%" + name + "%'";
        }
        if (!StringUtils.isNullOrEmpty(sex)) {
            sql += " and sex = '" + sex + "'";
        }
        if (!StringUtils.isNullOrEmpty(user_no)) {
            sql += " and user_no = '" + user_no + "'";
        }

        Integer count = this.queryCountForMysql(sql);
        return count;
    }

    public List<Object> queryEmplList(Integer start, Integer limit, String name, String sex, String user_no) throws Exception {
        String sql = "select * from user where 1 = 1 ";
        if (!StringUtils.isNullOrEmpty(name)) {
            sql += " and user_name like '%" + name + "%'";
        }
        if (!StringUtils.isNullOrEmpty(sex)) {
            sql += " and sex = '" + sex + "'";
        }
        if (!StringUtils.isNullOrEmpty(user_no)) {
            sql += " and user_no = '" + user_no + "'";
        }
        sql += " order by create_time desc limit " + start + "," + limit + "";
        return this.getDataBySqlForMysql(sql);
    }

    public void createEmpl(String name, String sex, String login_name, String passwd, String user_no, String email, String dept_name, String type) throws Exception {
        String insertImg = "insert into user (user_name,sex,login_name,passwd,type,email,dept_name,user_no,create_time) " +
                "values('" + name + "','" + sex + "','" + login_name + "','" + passwd + "','"+type+"','"+email+"','"+dept_name+"','" + user_no+"', '"+System.currentTimeMillis()+"')";
        this.updateForMysql(insertImg);
    }

    public void delEmplById(Integer id) throws Exception {
        String sql = "delete from user where id = " + id + "";
        this.updateForMysql(sql);
    }

    public List<Object> findAll() throws Exception {
        String sql = "select * from user where 1 = 1 ";
        sql += " order by id desc";
        return this.getDataBySqlForMysql(sql);
    }

    public Map<String, Object> queryById(Integer userId) throws Exception {
        String sql = "select * from user where id = " + userId + "";
        List<Object> list = this.getDataBySqlForMysql(sql);
        return list.size() != 0 ? (Map<String, Object>) list.get(0) : null;
    }
}
