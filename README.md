# employee_management

#### 介绍
企业员工信息管理 [服务]
毕业作品

### 环境
JDK1.8
Mysql5.6
Maven3

### 开发涉及工具
Idea2020
Navicat

### 使用技术
mysql+spring-boot+jdbc

### 配置
mysql
    项目下 qyyg.sql 为表结构和基本数据
    字符集 utf8mb4
    排序规则 utf8mb4_general_ci
startup.bat
    批处理文件，需要和 **employee_management-0.0.1.jar**放置再同一目录下
    端口号可以修改批处理文件里面的 **port**参数


### 启动
启动文件
    **employee_management-0.0.1.jar**
    **startup.bat**
双击 startup.bat即可启动，默认端口号 **8082**
访问地址为 http://localhost:8082
初始管理员账户/密码: admin/123456